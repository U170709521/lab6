package lab6;

public class Main {
	public static void main(String[] args) {
		Rectangle r = new Rectangle(5 , 8 , new Point(6,10)); 
		System.out.println(r.area());
		System.out.println(r.perimeter());
		Point[] pa = r.corner();
		for(int i = 0 ; i < pa.length ; i++) {
			System.out.println(pa[i].xCoord + " " + pa[i].yCoord);	
		}
		Circle c = new Circle(10, new Point(2,3));
		Circle B = new Circle(10, new Point(2,3));
		System.out.println(c.area());
		System.out.println(c.perimeter());
		if (c.intersect(B)) System.out.println(" Intersected ");
		else System.out.println(" Not intersected ");
		
	}
	
}