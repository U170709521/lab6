package lab6;

public class Circle {
	int radius ;
	
	Point center ;
	
	Circle (int x , Point A){
		radius = x ;
		center = A ;
	}
	
	double area() {
		double theArea = 3.14 * (radius * radius);
		return (theArea);
		
	}
	double perimeter() {
		double thePerimeter = 2 * (3.14 * radius);
		return (thePerimeter); 
	}
	boolean intersect(Circle A) {
		double d = (center.xCoord - A.center.xCoord) * (center.xCoord - A.center.xCoord) + 
				(center.yCoord - A.center.yCoord) * (center.yCoord - A.center.yCoord) * (0.5);
		int sum = radius + A.radius;
		if (d >= sum) return true ; 
		
		return false ; 
	}

}